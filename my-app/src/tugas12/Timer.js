import React, {Component} from 'react'

class Timer extends Component{
  constructor(props){
    super(props)
    this.state = {
      time: 100,
      date: new Date()
    }
  }

  componentDidMount(){
    if (this.props.start !== undefined){
      this.setState({time: this.props.start})
    }
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );

   
  }

  componentWillUnmount(){
    clearInterval(this.timerID);
    
  }

  tick() {
    this.setState({
      time: this.state.time - 1 ,
      date: new Date()
    });
  }


 

  render(){
    return(
      <>
      {this.state.time >=0 &&

<table style={{marginLeft: "auto",marginRight: "auto"}}>
<tr >
<th style={{paddingRight:"423px"}}>sekarang jam: {this.state.date.toLocaleTimeString('en-US', { hour12: true })} </th>
<th > hitungmundur: {this.state.time}</th>

</tr>
</table> 
     

      } 
    
    
      </>
    )
  }
}

export default Timer