import React, { useState, useEffect } from 'react';
import axios from 'axios';

const MovieForm = () => {
  const [daftarMovie, setDaftarMovie] =  useState([])
  const [inputTitle, setInputTitle]  =  useState("")
  const [inputDescription, setInputDescription]  =  useState("")
  const [inputYear, setInputYear]  =  useState(0)
  const [inputDuration, setInputDuration]  =  useState(0)
  const [inputGenre, setInputGenre]  =  useState("")
  const [inputRating, setInputRating]  =  useState(0)
  const [indexOfForm, setIndexOfForm] =  useState(-1)
  const [id, setId] =  useState(-1)

  useEffect(() => {
    if (daftarMovie.length === 0) {
      axios.get(`http://backendexample.sanbercloud.com/api/movies`)
        .then(res => {
          const newData = res.data.map((el) => { return el}) ;

          setDaftarMovie(newData);
          console.log(res)
          console.log(res)
          console.log(res)
        })
    }
  })


  const handleDelete = (event) => {
    let index = event.target.value
    let id = event.target.id;
    let newDaftarMovie = daftarMovie
    newDaftarMovie.splice(index, 1)

    setDaftarMovie([...newDaftarMovie])

    axios.delete(`http://backendexample.sanbercloud.com/api/movies/${id}`)
  }

  const handleEdit = (event) =>{
    let index = event.target.value;
    let id = event.target.id;
    let dataMovie = daftarMovie[index];


    setInputTitle(dataMovie.title)
    setInputDescription(dataMovie.description)
    setInputYear(dataMovie.year)
    setInputDuration(dataMovie.duration)
    setInputGenre(dataMovie.genre)
    setInputRating(dataMovie.rating)
    setIndexOfForm(index)
    setId(id);
  }

  const handleChangeTitle=(event)=>{
    setInputTitle(event.target.value)
  }
  const handleChangeDescription=(event)=>{
    setInputDescription(event.target.value)
  }
  const handleChangeYear=(event)=>{
    setInputYear(event.target.value)
  }
  const handleChangeDuration=(event)=>{
    setInputDuration(event.target.value)
  }
  const handleChangeGenre=(event)=>{
    setInputGenre(event.target.value)
  }
  const handleChangeRating=(event)=>{
    setInputRating(event.target.value)
  }


  function matchExpression( str ) {
    var rgularExp = {
        contains_alphaNumeric : /^(?!-)(?!.*-)[A-Za-z0-9-]+(?<!-)$/,
        containsNumber : /\d+/,
        containsAlphabet : /[a-zA-Z]/,
        onlyLetters : /^[A-Za-z\\,]+$/,
        onlyNumbers : /^[0-9]+$/,
        onlyMixOfAlphaNumeric : /^([0-9]+[a-zA-Z]+|[a-zA-Z]+[0-9]+)[0-9a-zA-Z]*$/
    }
    var expMatch = {};
    expMatch.containsNumber = rgularExp.containsNumber.test(str);
    expMatch.containsAlphabet = rgularExp.containsAlphabet.test(str);
    expMatch.alphaNumeric = rgularExp.contains_alphaNumeric.test(str);

    expMatch.onlyNumbers = rgularExp.onlyNumbers.test(str);
    expMatch.onlyLetters = rgularExp.onlyLetters.test(str);
    expMatch.mixOfAlphaNumeric = rgularExp.onlyMixOfAlphaNumeric.test(str);

    return expMatch;
}

  const handleSubmit = (event) =>{
    // menahan submit
    event.preventDefault()

    let title = inputTitle
    let description = inputDescription
    let year = inputYear
    let duration = inputDuration
    let genre = inputGenre
    let rating = inputRating
 


    if (title.replace(/\s/g,'') !== ""){
      let newDaftarMovie = daftarMovie
      let index = indexOfForm;
      let ids = id;
     
      if (index === -1 && inputRating>0 && inputRating<11  && matchExpression(inputGenre).onlyLetters && year<=2020 &&matchExpression(inputDuration).onlyNumbers){
        newDaftarMovie = [...newDaftarMovie, {title, description, year, duration, genre, rating}]
        axios.post(`http://backendexample.sanbercloud.com/api/movies`, { title, description, year, duration, genre, rating})
      }else if(inputRating>0 && inputRating<11  && matchExpression(inputGenre).onlyLetters && year<=2020 &&matchExpression(inputDuration).onlyNumbers){
        newDaftarMovie[index] = {title, description, year, duration, genre, rating}
        axios.put(`http://backendexample.sanbercloud.com/api/movies/${ids}`, {title, description, year, duration, genre, rating})
      }

      setDaftarMovie(newDaftarMovie)
      setInputTitle("")
      setInputDescription("")
      setInputYear(0)
      setInputDuration(0)
      setInputGenre("")
      setInputRating(0)
      setIndexOfForm(-1)
      setId(-1)
    }


  }

  return(
    <>
      <div className="body">
      <section className="sectiona">
        <h1 style={{textAlign:"center"}}>Data Film </h1>
                      <table style={{border: "2px solid black"}}>
                    
  
                          <tr style={{backgroundColor:"#ccc7c4"}}>
                          <th >no</th>
                          <th>Title</th>
                          <th>Description</th>
                          <th>Year</th>
                          <th>Duration</th>
                          <th>Genre</th>
                          <th>Rating</th>
                          <th>Action</th>
                          </tr>

                      {daftarMovie.map((el, index)=>{
                      return(
                              <tr style={{backgroundColor:"#f67833"}}>
                                  <td >{index+1}</td>
                                  <td style={{width:"100px"}}>{el.title}</td>
                                  <td style={{width:"800px"}}>{el.description}</td>
                                  <td style={{width:"40px"}}>{el.year}</td>
                                  <td style={{width:"40px"}}>{el.duration}</td>
                                  <td style={{width:"100px"}}>{el.genre}</td>
                                  <td style={{width:"10px"}}>{el.rating}</td>
                                  <td style={{width:"200px"}}> 
                                  <button id={el.id} onClick={handleEdit} value={index}> Edit </button>
                                  &nbsp;
                                  <button id={el.id} onClick={handleDelete} value={index}>Delete</button> 
                            </td>
                          </tr>
                          )})}
                          </table>
    {/* Form */}

    <h1 style={{textAlign:"center"}}>Form DataFilm</h1>
    <div style={{textAlign:"center"}}>
      
   
 <form onSubmit={handleSubmit}>
     <label>Title :</label><br/><input type="text" value={inputTitle} onChange={handleChangeTitle} />
    <br/>
     <label >Description :</label><br/>
     <textarea id="w3review" name="w3review" rows="4" cols="50" value={inputDescription} onChange={handleChangeDescription}>
      </textarea>
       <br/>
       <label>Year (tidak boleh lebih dari 2020):</label><br/>
      {/* <input type="text" value={inputDescription} onChange={handleChangeDescription}/> */}
      <input type="text" value={inputYear} onChange={handleChangeYear}/>
      <br/>
       <label>Duration (menit) :</label><br/>
      <input type="text" value={inputDuration} onChange={handleChangeDuration}/>
      <br/>
       <label>Genre(Rock,Metal,Fantasy) :</label><br/>
      <input type="text" value={inputGenre} onChange={handleChangeGenre}/>
      <br/>
       <label>Rating(1-10) :</label><br/>
      <input type="text" value={inputRating} onChange={handleChangeRating}/>
      <br/><button >submit</button>
    </form>
   

 
     </div>
    
     </section>
 

     
      <footer className="footer">
      <h5>copyright © 2020 by Sanbercode</h5>
    </footer>
    </div>
    </>
  )
}

export default MovieForm
