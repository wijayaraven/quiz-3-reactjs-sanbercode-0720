
import React, {useContext} from "react";
import logo from "./public/img/logo.png"
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";

import Login from "./Login";
import Home from "./Home";
import About from "./About";
import MovieForm from "./MovieForm";
import style from "./public/css/style.css"
import {UserContext} from "./UserContext"
const Routes = () => {
  const [
    daftarUser, setDaftarUser,
    statusLogin, setStatusLogin,
    inputUsername, setInputUsername,
    inputPassword, setInputPassword,] = useContext(UserContext);
    
    const handlelogout = () => {
        setStatusLogin(0)
      }
     
      
   return (
     
<Router>
      <div>
      <div>
        <img id="logo" src={logo} width="200px"  />
            <ul className="ul">
       
            { statusLogin === 1 ? (
             
            <>
               <li className="li"><Link to="/Home">Home</Link> </li>
               <li className="li"><Link to="/About">About</Link></li>
               <li className="li"><Link to="/MovieForm">Movie List Editor</Link> </li>
               <li className="li" onClick={handlelogout}><Link to="/">Logout</Link> </li> 
     
              </>
                ) : (
                <>
                <li className="li"><Link to="/Home">Home</Link> </li>
                <li className="li"><Link to="/About">About</Link></li>
                <li className="li"><Link to="/">Login</Link> </li> 
                
                </>

                
                )
                }   

            </ul>
        </div>


        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
        
        { statusLogin === 1 ? (
             
             <>
              <Route exact path="/Home">
                <Home />
              </Route> 

              <Route exact path="/MovieForm">
                <MovieForm />
              </Route>

              <Route exact path="/About">
                  <About />
              </Route> 
         
              <Route exact path="/">
                  <Login />
              </Route> 
      
               </>
                 ) : (
                <>   
                <Route exact path="/Home">
                <Home />
              </Route> 
                  <Route exact path="/About">
                     <About />
                  </Route> 
             
                  <Route exact path="/">
                     <Login />
                  </Route> 
                </>
                 
                 )
                 }  
         
        </Switch>
      </div>
</Router>
     
   
  );
};

export default Routes;