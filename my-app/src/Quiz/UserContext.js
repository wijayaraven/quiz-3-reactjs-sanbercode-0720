import React, { useState, createContext, useEffect } from "react";
import axios from 'axios';

export const UserContext = createContext();

export const UserProvider = props => {
   const [daftarUser, setDaftarUser] =  useState([])
  const [inputUsername, setInputUsername]  = useState(null)
  const [inputPassword, setInputPassword]  = useState(null)
  const [statusLogin, setStatusLogin]  =  useState(0)
  

  // useEffect(() => {
  //   if (daftarUser.length === 0) {
  //     axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
  //       .then(res => {
  //         const newData = res.data.map((el) => { return el});
  //         setDaftarUser(newData);
  //       })
  //   }
  // })

  return (
    <UserContext.Provider value={[daftarUser, setDaftarUser,
                                  inputUsername, setInputUsername,
                                  inputPassword, setInputPassword,
                                  statusLogin, setStatusLogin,
                                 ]}>
        {props.children}
    </UserContext.Provider>
  );
};







