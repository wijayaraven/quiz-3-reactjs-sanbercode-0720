import React, { useState, createContext, useEffect } from "react";
import axios from 'axios';

export const MovieContext = createContext();

export const MovieProvider = props => {
  
  const [daftarMovie, setDaftarMovie] =  useState([])
  const [inputTitle, setInputTitle]  =  useState("")
  const [inputDescription, setInputDescription]  =  useState("")
  const [inputYear, setInputYear]  =  useState(0)
  const [inputDuration, setInputDuration]  =  useState(0)
  const [inputGenre, setInputGenre]  =  useState("")
  const [inputRating, setInputRating]  =  useState(0)
  const [indexOfForm, setIndexOfForm] =  useState(-1)
  const [id, setId] =  useState(-1)

  useEffect(() => {
    if (daftarMovie.length === 0) {
      axios.get(`http://backendexample.sanbercloud.com/api/movies`)
        .then(res => {
          const newData = res.data.map((el) => { return el});
          setDaftarMovie(newData);
        })
    }
  })

  return (
    <MovieContext.Provider value={[
                                  daftarMovie, setDaftarMovie,
                                  inputTitle, setInputTitle,
                                  inputDescription, setInputDescription,
                                  inputYear, setInputYear,
                                  inputDuration, setInputDuration,
                                  inputGenre, setInputGenre,
                                  inputRating, setInputRating,
                                  indexOfForm, setIndexOfForm,
                                  id, setId
                                 ]}>
        {props.children}
    </MovieContext.Provider>
  );
};
