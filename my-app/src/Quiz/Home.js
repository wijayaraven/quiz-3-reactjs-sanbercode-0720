import React, { Component} from 'react';
import axios from 'axios';
import {MovieContext} from "./MovieContext"
class Home extends Component{
  constructor(props){
    super(props)
    this.state = {
      daftarMovie:[]
    }

  }

  componentDidMount(){
    if(this.state.daftarMovie.length===0){
      axios.get(`http://backendexample.sanbercloud.com/api/movies`)
      .then(res => {
        this.setState({daftarMovie:res.data.sort(function(a, b){
          return b.rating-a.rating
      }) })
        console.log(this.state.daftarMovie)

      })
    }
  }
  
 

    
render(){

return(
  <> 
   
    
   <div className="body">
      <section className="sectiona">
        <div>
        <h1 style={{textAlign:"center",fontSize:"40px"}}>Daftar Film Film Terbaik </h1>
        <ul>
          {this.state.daftarMovie.map(el => (
             <div>
             <h1 key={el.id} style={{color:"blue"}}>{el.title}</h1>
             <p key={el.id} ><b>Rating {el.rating}</b> </p>
             <p key={el.id}><b>Durasi :{Math.floor((el.duration / 60))} jam {Math.round(((el.duration / 60) - Math.floor(el.duration / 60)) * 60)} menit</b></p>
             {el.genre!==null ? (
              <p key={el.id}><b>Genre: {el.genre.split(",").map(el =>(
                <p style={{display:"inline"}}><b>&nbsp;&bull;{el} </b></p>
                ))}</b></p>
                ) : (
                  <p><b>Genre :</b>Tidak ada Genre</p>
                )}
             
           
             <p key={el.id}><b>Deskripsi: </b>{el.description} </p>
             <hr/>
           </div>
          ))}
        </ul>
       
        </div>
     </section>
     
     <footer className="footer">
      <h5>copyright © 2020 by Sanbercode</h5>
    </footer>
     </div>
 
  </>
)

  }
}


export default Home

