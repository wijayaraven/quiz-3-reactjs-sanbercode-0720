import React from 'react';
// import './App.css';
import { BrowserRouter as Router } from "react-router-dom";
import Routes from "./Quiz/Route"
import Home from './Quiz/Home';
import {UserProvider} from "./Quiz/UserContext"
function App() {
  return (
    <div>
  
    <Router>  <UserProvider>
      <Routes/>   </UserProvider>
    </Router>  
 
 
  
    </div>
   
  );
}

export default App;