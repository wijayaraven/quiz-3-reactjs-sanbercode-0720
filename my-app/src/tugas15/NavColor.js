import React from "react"
import {ColorNavProvider} from "./ColorNavContext"
import NavColorList from "./NavColorList"

const ColorNav = () =>{
  return(
    <ColorNavProvider>
      <NavColorList/>
    </ColorNavProvider>
  )
}

export default ColorNav
