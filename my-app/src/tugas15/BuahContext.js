import React, { useState, createContext, useEffect } from "react";
import axios from 'axios';

export const BuahContext = createContext();

export const BuahProvider = props => {
  const [daftarBuah, setDaftarBuah] =  useState([])
  const [inputNamaBuah, setInputNamaBuah]  =  useState("")
  const [inputHargaBuah, setInputHargaBuah]  =  useState(0)
  const [inputBeratBuah, setInputBeratBuah]  =  useState(0)
  const [indexOfForm, setIndexOfForm] =  useState(-1)
  const [id, setId] =  useState(-1)

  useEffect(() => {
    if (daftarBuah.length === 0) {
      axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
        .then(res => {
          const newData = res.data.map((el) => { return el});
          setDaftarBuah(newData);
        })
    }
  })

  return (
    <BuahContext.Provider value={[
                                  daftarBuah, setDaftarBuah,
                                  inputNamaBuah, setInputNamaBuah,
                                  inputHargaBuah, setInputHargaBuah,
                                  inputBeratBuah, setInputBeratBuah,
                                  indexOfForm, setIndexOfForm,
                                  id, setId
                                 ]}>
        {props.children}
    </BuahContext.Provider>
  );
};
