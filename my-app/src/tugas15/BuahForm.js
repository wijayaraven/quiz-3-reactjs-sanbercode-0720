import React, {useContext} from "react";
import {BuahContext} from "./BuahContext";
import axios from 'axios';

const BuahForm = () =>{
  const [
    daftarBuah, setDaftarBuah,
    inputNamaBuah, setInputNamaBuah,
    inputHargaBuah, setInputHargaBuah,
    inputBeratBuah, setInputBeratBuah,
    indexOfForm, setIndexOfForm,
    id, setId
  ] = useContext(BuahContext);

  const handleSubmit = (event) => {

    event.preventDefault()

    let name = inputNamaBuah
    let price = inputHargaBuah
    let weight = inputBeratBuah


    if (name.replace(/\s/g,'') !== ""){
      let newDaftarBuah = daftarBuah
      let index = indexOfForm;
      let ids = id;

      console.log(indexOfForm, ids);

      if (index === -1){
        newDaftarBuah = [...newDaftarBuah, {name, price, weight}]
        axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name, price, weight})
      }else{
        newDaftarBuah[index] = {name, price, weight}
        axios.put(`http://backendexample.sanbercloud.com/api/fruits/${ids}`, {name, price, weight})
      }

      setDaftarBuah(newDaftarBuah)
      setInputNamaBuah("")
      setInputHargaBuah(0)
      setInputBeratBuah(0)
      setIndexOfForm(-1)
      setId(-1)
    }

  }
  const handleChangeNama=(event)=>{
    setInputNamaBuah(event.target.value)
  }
  const handleChangeHarga=(event)=>{
    setInputHargaBuah(event.target.value)
  }
  const handleChangeBerat=(event)=>{
    setInputBeratBuah(event.target.value)
  }
 
  return(
    <>
    <h1 style={{textAlign:"center"}}>Form DataBuah</h1>
      <div style={{marginLeft:"350px"}}>
      <table>
      <th style={{paddingRight:"100px"}}>nama</th>
      <th style={{paddingRight:"100px"}}>harga</th>
      <th>berat</th>
      </table>
      
      <form onSubmit={handleSubmit}>
        <input type="text" value={inputNamaBuah} onChange={handleChangeNama}/>
        <input type="text" value={inputHargaBuah} onChange={handleChangeHarga}/>
        <input type="text" value={inputBeratBuah} onChange={handleChangeBerat}/>
        <button>submit</button>
      </form>
      </div>

    </>
  )

}

export default BuahForm
