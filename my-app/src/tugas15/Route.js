import React from "react";
import Home from "./home";

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";

import Tugas11 from "../tugas11/TabelBuah";
import Tugas12 from "../tugas12/Timer";
import Tugas13 from "../tugas13/List";
import Tugas14 from "../tugas14/List";
import Tugas15 from "./tugas15";
const Routes = () => {

   return (
    <Router>
      <div>
      <nav class="navbar navbar-expand-lg navbar-light bg-light">

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul style={{textAlign:"center"}}>
            <li style={{  
              fontweight: "bold",
              display:"inline-block",
              padding: "10px"}}>
              <Link to="/">Home</Link>
            </li>
            <li style={{  
              fontweight: "bold",
              display:"inline-block",
              padding: "10px"}}>
              <Link to="/Tugas11">Tugas11</Link>
            </li >
            <li style={{  
              fontweight: "bold",
              display:"inline-block",
              padding: "10px"}}>
              <Link to="/Tugas12">Tugas12</Link>
            </li>
            <li style={{  
              fontweight: "bold",
              display:"inline-block",
              padding: "10px"}}>
              <Link to="/Tugas13">Tugas13</Link>
            </li>
            <li style={{  
              fontweight: "bold",
              display:"inline-block",
              padding: "10px"}}>
              <Link to="/Tugas14">Tugas14</Link>
            </li>
            <li style={{  
              fontweight: "bold",
              display:"inline-block",
              padding: "10px"}}>
              <Link to="/Tugas15">Tugas15</Link>
            </li>
    </ul>
  
  </div>
</nav>



        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/Tugas11">
            <Tugas11 />
          </Route>
          <Route path="/Tugas12">
            <Tugas12 />
          </Route> 
          <Route path="/Tugas13">
            <Tugas13 />
          </Route>
          <Route path="/Tugas14">
            <Tugas14 />
          </Route>
          <Route path="/Tugas15">
            <Tugas15 />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
};

export default Routes;